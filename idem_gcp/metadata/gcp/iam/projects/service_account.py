"""Metadata module for managing ServiceAccounts."""

PATH = "projects/{project}/serviceAccounts/{serviceAccount}"

NATIVE_RESOURCE_TYPE = "iam.projects.service_accounts"
