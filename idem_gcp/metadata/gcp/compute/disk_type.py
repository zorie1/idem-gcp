"""Metadata module for managing Disk Types."""

PATH = [
    "projects/{project}/zones/{zone}/diskTypes/{diskType}",
    "projects/{project}/regions/{region}/diskTypes/{diskType}",
]

NATIVE_RESOURCE_TYPE = [
    "compute.disk_types",
    "compute.region_disk_types",
]
