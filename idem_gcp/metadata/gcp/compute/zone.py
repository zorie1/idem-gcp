"""Metadata module for managing Zones."""

PATH = "projects/{project}/zones/{zone}"

NATIVE_RESOURCE_TYPE = "compute.zones"
