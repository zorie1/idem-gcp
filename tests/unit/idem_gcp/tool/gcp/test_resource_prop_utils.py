from unittest.mock import MagicMock

import pytest


def test_parse_link_to_resource_id_invalid_link(hub):
    self_link = "https://www.googleapis.com/compute/v1/projects/test-project/regions1/test-region/diskTypes/type-name"
    resource_type = "compute.disk_type"

    result = hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
        self_link, resource_type
    )

    assert not result


def test_parse_link_to_resource_id_valid_link(hub):
    self_link = "https://www.googleapis.com/compute/v1/projects/test-project/zones/test-zone/diskTypes/type-name"
    resource_type = "compute.disk_type"
    result = hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
        self_link, resource_type
    )

    assert result == "projects/test-project/zones/test-zone/diskTypes/type-name"


@pytest.mark.parametrize(
    "resource_id,matches",
    [
        (
            "https://www.googleapis.com/compute/v1/projects/test-project/zones/test-zone/diskTypes/type-name",
            True,
        ),
        (
            "https://www.googleapis.com/compute/v1/projects/test-project/zones/test-zone/diskTypes0/type-name",
            False,
        ),
        ("/projects/test-project/zones/test-zone/diskTypes/type-name", True),
        ("projects/test-project/zones/test-zone/diskTypes/type-name", True),
        ("projects/test-project/zones/test-zone/diskTypes/type-name0", True),
        ("test-project/zones/test-zone/diskTypes0/type-name", False),
        ("type-name", False),
    ],
)
def test_resource_type_matches(hub, resource_id, matches):
    assert matches == hub.tool.gcp.resource_prop_utils.resource_type_matches(
        resource_id, "compute.disk_type"
    )


def test_construct_resource_id_exact_props(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    props = {"project": project, "zone": zone, "instance": instance}
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_path = MagicMock(
        return_value=resource_id_path
    )
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_construct_resource_id_missing_props(hub):
    project = "test-project"
    zone = "test-zone"
    props = {
        "project": project,
        "zone": zone,
    }
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_path = MagicMock(
        return_value=resource_id_path
    )
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id is None


def test_construct_resource_id_redundant_props(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    props = {
        "project": project,
        "zone": zone,
        "instance": instance,
        "redundant": "redundant",
    }
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_path = MagicMock(
        return_value=resource_id_path
    )
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_construct_resource_id_no_props(hub):
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_path = MagicMock(
        return_value=resource_id_path
    )
    resource_type = "compute.instance"

    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, None
    )
    assert resource_id is None


def test_construct_resource_id_name_contains_id(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    props = {
        "name": expected_resource_id,
        "project": "dummy",
        "zone": "dummy",
        "instance": "dummy",
    }
    hub.tool.gcp.resource_prop_utils.get_resource_path = MagicMock(
        return_value=resource_id_path
    )
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_construct_resource_id_simple_name(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    props = {
        "name": instance,
        "project": project,
        "zone": zone,
        "instance": instance,
    }
    resource_id_path = "projects/{project}/zones/{zone}/instances/{instance}"
    hub.tool.gcp.resource_prop_utils.get_resource_path = MagicMock(
        return_value=resource_id_path
    )
    expected_resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    resource_type = "compute.instance"
    resource_id = hub.tool.gcp.resource_prop_utils.construct_resource_id(
        resource_type, props
    )
    assert resource_id == expected_resource_id


def test_get_elements_from_resource_id(hub):
    resource_type = "compute.snapshot"
    resource_paths = hub.tool.gcp.resource_prop_utils.get_resource_paths(resource_type)
    project = "test-project"
    snapshot = "test-snapshot"
    with pytest.raises(ValueError) as e_info:
        hub.tool.gcp.resource_prop_utils.get_elements_from_resource_id(
            "compute.snapshot", f"projects/{project}/global/snapshot/{snapshot}"
        )
    assert str(
        e_info.value
    ) == hub.tool.gcp.comment_utils.ill_formed_resource_id_comment(
        resource_type, f"projects/{project}/global/snapshot/{snapshot}", resource_paths
    )

    els = hub.tool.gcp.resource_prop_utils.get_elements_from_resource_id(
        "compute.snapshot", f"projects/{project}/global/snapshots/{snapshot}"
    )
    assert els is not None
    assert els["project"] == project
    assert els["snapshot"] == snapshot


def test_properties_mismatch_resource_id(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    resource_type = "compute.instance"
    state_properties = {
        "project": project,
        "zone": zone,
        "instance": instance,
    }
    resource_id = f"projects/{project}/zones/asia/instances/{instance}"
    expected_mismatch = (
        hub.tool.gcp.resource_prop_utils.properties_mismatch_resource_id(
            resource_type, resource_id, state_properties
        )
    )

    assert expected_mismatch


def test_properties_no_mismatch_resource_id(hub):
    project = "test-project"
    zone = "test-zone"
    instance = "test-instance"
    resource_type = "compute.instance"
    state_properties = {
        "project": project,
        "zone": zone,
        "instance": instance,
    }
    resource_id = f"projects/{project}/zones/{zone}/instances/{instance}"
    expected_mismatch = (
        hub.tool.gcp.resource_prop_utils.properties_mismatch_resource_id(
            resource_type, resource_id, state_properties
        )
    )

    assert not expected_mismatch
