def test_snake(hub):
    data = [("retentionPolicy", "retention_policy")]
    for x in data:
        assert hub.tool.gcp.case.snake(x[0]) == x[1]
        assert hub.tool.gcp.case.camel(x[1], True) == x[0]


def test_is_unclashed(hub):
    assert hub.tool.gcp.case.is_unclashed("id") is False
    assert hub.tool.gcp.case.is_unclashed("id_") is True
    assert hub.tool.gcp.case.is_unclashed("non_builtin") is True
    assert hub.tool.gcp.case.is_unclashed("type") is False
    assert hub.tool.gcp.case.is_unclashed("type_") is True
    assert hub.tool.gcp.case.is_unclashed("TYPE") is True
    assert hub.tool.gcp.case.is_unclashed("CAPITALS") is True
    assert hub.tool.gcp.case.is_unclashed("CamelCase") is True


def test_sanitize_key(hub) -> str:
    assert hub.tool.gcp.case.sanitize_key("id") == "id_"
    assert hub.tool.gcp.case.sanitize_key("type") == "type_"
    assert hub.tool.gcp.case.sanitize_key("camelCase") == "camel_case"
