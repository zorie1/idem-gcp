import copy


async def test_update_network_interfaces_validation(hub, ctx):
    old_state = {
        "network_interfaces": [
            {
                "name": "nic0",
                "network": "projects/project-name/global/networks/default",
            }
        ]
    }
    new_interfaces = [
        {
            "name": "nic1",
            "network": "projects/project-name/global/networks/default",
        },
        {
            "name": "nic2",
            "network": "projects/project-name/global/networks/default",
        },
    ]
    result = await hub.tool.gcp.compute.instance.update_network_interfaces(
        ctx, old_state, new_interfaces
    )
    assert result["result"] is False
    assert result["comment"] == [
        "Network interface addition not supported for interface nic1",
        "Network interface addition not supported for interface nic2",
        "Network interface deletion not supported for interface nic0",
    ]


async def test_update_network_interfaces_too_many_access_configs(hub, ctx):
    old_state = {
        "network_interfaces": [
            {
                "name": "nic0",
                "network": "projects/project-name/global/networks/default",
                "access_configs": [{"name": "External NAT", "type_": "ONE_TO_ONE_NAT"}],
            }
        ]
    }
    new_interfaces = [
        {
            "name": "nic0",
            "network": "projects/project-name/global/networks/default",
            "access_configs": [
                {"name": "External NAT", "type_": "ONE_TO_ONE_NAT"},
                {"name": "other config"},
            ],
        }
    ]
    result = await hub.tool.gcp.compute.instance.update_network_interfaces(
        ctx, old_state, new_interfaces
    )
    assert result["result"] is False
    assert result["comment"] == [
        "Network interface nic0 cannot have more than one access config"
    ]


async def test_update_network_interfaces_valid_no_changes(hub, ctx):
    network_interfaces = [
        {
            "name": "nic0",
            "network": "projects/project-name/global/networks/default",
        }
    ]
    old_state = {"network_interfaces": network_interfaces}
    new_interfaces = copy.deepcopy(network_interfaces)
    result = await hub.tool.gcp.compute.instance.update_network_interfaces(
        ctx, old_state, new_interfaces
    )
    assert result["result"] is True
    assert result["comment"] == []
