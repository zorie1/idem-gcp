import os
from typing import Set


ESSENTIAL_GCP_RESOURCES: Set[str] = {
    "cloudkms.crypto_key",
    "cloudkms.crypto_key_version",
    "cloudkms.import_job",
    "cloudkms.key_ring",
    "compute.disk",
    "compute.image",
    "compute.instance",
    "compute.machine_image",
    "compute.network",
    "compute.snapshot",
    "compute.subnetwork",
}


def test_schema_compatibility(
    hub, idem_cli, official_repo_url, current_rev, current_states_schema
):
    base_rev = os.environ.get("BASE_SCHEMA_REV") or "v0.9.0"
    base_schema = hub.tool.gcp.schema.scm_utils.fetch_states_schema_from_official_repo(
        official_repo_url, base_rev
    )

    collector = hub.tool.gcp.schema.schema_compatibility_checker.check(
        current_states_schema, base_schema, ESSENTIAL_GCP_RESOURCES
    )

    compatibility_report = collector.revisions(current_rev, base_rev).report_as_json()
    with open("compatibility-report.json", "w") as f:
        f.write(compatibility_report)

    print(f"\nSchema compatibility check completed ({base_rev} -> {current_rev})")


def test_states_naming(hub, current_rev, current_states_schema):
    collector = hub.tool.gcp.schema.schema_naming_checker.check(current_states_schema)

    naming_report = collector.revisions(current_rev, None).report_as_json()
    with open("states-naming-report.json", "w") as f:
        f.write(naming_report)

    print(f"\nStates schema naming check completed ({current_rev})")

    if collector.has_breaking_finding():
        print(naming_report)
        assert False


def test_exec_naming(hub, current_rev, current_exec_schema):
    collector = hub.tool.gcp.schema.schema_naming_checker.check(current_exec_schema)

    naming_report = collector.revisions(current_rev, None).report_as_json()
    with open("exec-naming-report.json", "w") as f:
        f.write(naming_report)

    print(f"\nExec schema naming check completed ({current_rev})")

    if collector.has_breaking_finding():
        print(naming_report)
        assert False
