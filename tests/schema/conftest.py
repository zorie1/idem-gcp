import os

import pytest


@pytest.fixture(scope="session", name="hub")
def schema_hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="module")
def official_repo_url():
    return "https://gitlab.com/vmware/idem/idem-gcp.git"


@pytest.fixture(scope="module")
def current_rev():
    return os.environ.get("CI_COMMIT_SHA") or "HEAD"


@pytest.fixture(scope="module")
def current_states_schema(hub, idem_cli):
    return hub.tool.gcp.schema.scm_utils.fetch_current_states_schema(idem_cli)


@pytest.fixture(scope="module")
def current_exec_schema(hub, idem_cli):
    return hub.tool.gcp.schema.scm_utils.fetch_current_exec_schema(idem_cli)
