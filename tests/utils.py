"""Creates and deletes GCP instance using the

Usage pattern taken from: https://github.com/GoogleCloudPlatform/python-docs-samples/blob/81a8413528860ff368a3e9f3b7f4e3665588d07f/compute/api/create_instance.py

Running the methods requires the GOOGLE_APPLICATION_CREDENTIALS environment variable
to contain the file path to a service account credentials JSON file
- as obtained by the GCP console
"""
import os
import random
import time
import uuid
from typing import Any
from typing import Dict

import googleapiclient
from deepdiff import DeepDiff
from dict_tools.data import NamespaceDict


chars = "0123456789abcdefghijklmnopqrstuvwxyz"


def create_external_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        source_disk_image = (
            "projects/debian-cloud/global/images/debian-11-bullseye-v20221102"
        )
        config = {
            "name": name,
            "machineType": "projects/tango-gcp/zones/us-central1-a/machineTypes/g1-small",
            "disks": [
                {
                    "boot": True,
                    "autoDelete": True,
                    "initializeParams": {
                        "sourceImage": source_disk_image,
                    },
                }
            ],
            "networkInterfaces": [
                {
                    "network": "projects/tango-gcp/global/networks/default",
                    "accessConfigs": [
                        {"type_": "ONE_TO_ONE_NAT", "name": "External NAT"}
                    ],
                }
            ],
        }
        return (
            compute_service.instances()
            .insert(project=project, zone=zone, body=config)
            .execute()
        )


def stop_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        return (
            compute_service.instances()
            .stop(project=project, zone=zone, instance=name)
            .execute()
        )


def delete_instance(ctx, project: str, zone: str, name: str):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        return (
            compute_service.instances()
            .delete(project=project, zone=zone, instance=name)
            .execute()
        )


def wait_for_operation(ctx, project, zone, operation):
    with googleapiclient.discovery.build(
        serviceName="compute", version="v1", credentials=ctx.acct["credentials"]
    ) as compute_service:
        while True:
            result = (
                compute_service.zoneOperations()
                .get(project=project, zone=zone, operation=operation["id"])
                .execute()
            )

            if result["status"] == "DONE":
                if "error" in result:
                    raise Exception(result["error"])
                return result

            time.sleep(5)


def compare_expected_state(
    hub,
    actual_state: Dict[str, Any],
    expected_state: Dict[str, Any],
    resource_type: str,
):
    filtered_expected_state = {**expected_state}
    for key, value in expected_state.items():
        if value is None and key not in actual_state:
            filtered_expected_state.pop(key)

    exclude_paths = []
    if resource_type:
        exclude_paths = hub.tool.gcp.utils.get_deep_diff_exclude_paths(resource_type)

    changes = DeepDiff(
        actual_state,
        filtered_expected_state,
        exclude_regex_paths=exclude_paths,
        ignore_type_in_groups=[(NamespaceDict, dict)],
    )
    hub.tool.gcp.utils.filter_diff(changes, filtered_expected_state)

    if (
        not changes.get("dictionary_item_added")
        and not changes.get("iterable_item_added")
        and not changes.get("iterable_item_removed")
        and not changes.get("values_changed")
    ):
        changes = {}

    assert not bool(changes), changes


def __to_base(s, b):
    res = ""
    while s:
        res += chars[s % b]
        s //= b
    return res[::-1] or "0"


def generate_unique_name(name_prefix: str, max_length: int = 62) -> str:
    r"""Uses the CI_MERGE_REQUEST_IID and JOB_ID environment variable to generate unique name.
    This can help associate the resource with the pipeline that has created it. If the test is
    run locally the second part is missing:
        {name_prefix}{MERGE_REQUEST_IID-JOB_ID}?-{random-part}

    Args:
        name_prefix: name prefix between 2 and 42 characters.
        max_length: the maximum number of characters of the returned name. Default is 62.

    Returns:
        random name up to 62 characters in length. The returned name guaranteed will not end
        with '-' but with number or letter.

    Examples:
        generate_unique_name(None) -> ValueError("name is None")
        generate_unique_name('') -> ValueError("name is too short")
        generate_unique_name('this-is-a-very-long-name-prefix-0-1-2-3-4-5') -> ValueError("name is too long")

        standalone:  generate_unique_name('idem-test') -> idem-gcp-3m2422oxdcygx87wvmo3pffkw
        standalone:  generate_unique_name('idem-test', 20) -> idem-gcp-3m2422oxdcy
        in pipeline: generate_unique_name('idem-test') -> idem-test-123-3997117126-3m2422oxdcygx87wvmo3pffkw
    """

    if not name_prefix:
        raise ValueError("name is None")

    if len(name_prefix) < 2:
        raise ValueError("name is too short: " + name_prefix)

    if len(name_prefix) > 42:
        raise ValueError("name is too long: " + name_prefix)

    if max_length < 10:
        raise ValueError(
            "max_length bigger than 9 is required, current is: " + max_length
        )

    result = name_prefix + "-"

    merge_request_id = os.environ.get("CI_MERGE_REQUEST_IID")
    if merge_request_id:
        result += merge_request_id.lower() + "-"

    job_id = os.environ.get("CI_JOB_ID")
    if job_id:
        result += job_id.lower() + "-"

    result += __to_base(uuid.uuid4().int, 36)
    result = result[:max_length]

    if result[-1] == "-":
        result = result[:-1] + random.choice(chars)

    return result
