from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.iam.projects.service_accounts.key.describe(ctx)
    for resource_id in ret:
        assert "gcp.iam.projects.service_accounts.key.present" in ret[resource_id]
        described_resource = ret[resource_id].get(
            "gcp.iam.projects.service_accounts.key.present"
        )
        assert described_resource
        service_account_key = dict(ChainMap(*described_resource))
        assert service_account_key.get("resource_id") == resource_id
