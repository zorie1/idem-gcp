import random
from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
NAME = generate_unique_name("idem-account", 30)
PARAMETER = {
    "name": NAME,
    "account_id": NAME,
    "description": "Created by automated gcp-idem IT for service accounts.",
    "display_name": "idem-gcp-it",
}
RESOURCE_TYPE = "iam.projects.service_account"
PRESENT_CREATE_STATE = {**PARAMETER}


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.iam.projects.service_account.describe(ctx)
    for resource_id in ret:
        assert "gcp.iam.projects.service_account.present" in ret[resource_id]
        described_resource = ret[resource_id].get(
            "gcp.iam.projects.service_account.present"
        )
        assert described_resource
        service_account = dict(ChainMap(*described_resource))
        assert service_account.get("resource_id") == resource_id


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_present_create(hub, idem_cli, tests_dir, __test, cleaner):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], "iam.projects.service_account")

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}",
                name=ret["new_state"]["resource_id"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}",
                name=ret["new_state"]["resource_id"],
            )
            in ret["comment"]
        )


@pytest.mark.dependency(name="present_update")
def test_service_account_present_update(hub, idem_cli, tests_dir, cleaner):
    global PARAMETER
    PRESENT_CREATE_STATE["account_id"] = "account-" + str(random.randint(1111, 9999))
    PRESENT_CREATE_STATE["name"] = PRESENT_CREATE_STATE["account_id"]
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], "iam.projects.service_account")

    update_state = {
        **ret["new_state"],
        "description": "update operation applied",
        "display_name": "idem-gcp-it-update",
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_state
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    assert update_state["description"] == ret["new_state"]["description"]
    assert update_state["display_name"] == ret["new_state"]["display_name"]
