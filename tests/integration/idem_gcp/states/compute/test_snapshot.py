from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE_SNAPSHOT = "compute.snapshot"
PARAMETER = {
    "snapshot_name": generate_unique_name("idem-test-snapshot"),
    "disk_name": generate_unique_name("idem-test-disk"),
}

SNAPSHOT_CREATE_PROPERTIES = {
    "storage_locations": ["us-central1"],
    "description": "test description",
}

LABELS_TO_ADD = {"labels": {"label1": "value1", "label2": "value2"}}

CHANGED_LABELS = {"labels": {"label1": "value1changed", "label2": "value1changed"}}

LABELS_TO_CHANGE_AND_ADD = {
    "labels": {
        "label1": "value1changedagain",
        "label2": "value1changedagain",
        "label3": "value3",
    }
}

LABELS_TO_REMOVE = {"labels": {"label1": "value1changedagain"}}

CHANGED_NON_UPDATABLE_PROPERTIES = {
    "description": "test",
    "source_disk": "projects/example-propject/zones/us-central1-a/disks/example-disk",
    "snapshot_type": "ARCHIVE",
}


@pytest.fixture(scope="module", autouse=True)
def setup(hub, ctx):
    # This code is run once before all tests
    global PARAMETER
    PARAMETER["project"] = hub.tool.gcp.utils.get_project_from_account(ctx)
    PARAMETER[
        "resource_id"
    ] = f"projects/{PARAMETER['project']}/global/snapshots/{PARAMETER['snapshot_name']}"
    PARAMETER[
        "read_only_props"
    ] = hub.tool.gcp.resource_prop_utils.get_readonly_return_props(
        RESOURCE_TYPE_SNAPSHOT
    )


@pytest.fixture(scope="module")
def new_disk(hub, ctx, idem_cli):
    present_state_disk = {
        "name": PARAMETER["disk_name"],
        "project": PARAMETER["project"],
        "zone": "us-central1-a",
        "type_": f"projects/{PARAMETER['project']}/zones/us-central1-a/diskTypes/pd-balanced",
        "size_gb": "10",
        "physical_block_size_bytes": "4096",
        "resource_policies": [
            f"projects/{PARAMETER['project']}/regions/us-central1/resourcePolicies/default-schedule-1"
        ],
        "guest_os_features": [{"type_": "UEFI_COMPATIBLE"}],
    }

    disk_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, "compute.disk", present_state_disk
    )
    assert disk_present_ret["result"], disk_present_ret["comment"]

    resource_id = disk_present_ret["new_state"]["resource_id"]
    expected_state = {"resource_id": resource_id, **present_state_disk}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state,
        disk_present_ret["new_state"],
        "compute.disk",
        hub.tool.gcp.resource_prop_utils.get_readonly_return_props("compute.disk"),
    )
    assert not bool(changes), changes

    PARAMETER["source_disk"] = resource_id

    yield disk_present_ret

    disk_absent_ret = hub.tool.utils.call_absent(
        idem_cli, "compute.disk", disk_present_ret["new_state"]["name"], resource_id
    )
    assert disk_absent_ret["result"], disk_absent_ret["comment"]
    assert not disk_absent_ret.get("new_state")


@pytest.mark.asyncio
@pytest.mark.dependency(name="present_create")
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_snapshot_create(hub, idem_cli, __test, new_disk):
    create_state = {
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, create_state, __test
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )

    expected_state = {"resource_id": PARAMETER["resource_id"], **create_state}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state,
        ret["new_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id", depends=["present_create"]
)
def test_snapshot_present_get_resource_only_with_resource_id(hub, idem_cli, __test):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "resource_id": PARAMETER["resource_id"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_SNAPSHOT,
        state_to_enforce,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
            name=PARAMETER["snapshot_name"],
        )
    ] == ret["comment"]

    expected_state = {"resource_id": PARAMETER["resource_id"], **state_to_enforce}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state,
        ret["new_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_snapshot_present_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, __test
):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_SNAPSHOT,
        state_to_enforce,
        __test,
        ["--get-resource-only-with-resource-id"],
    )
    assert not ret["old_state"]

    if __test:
        assert ret["result"], ret["comment"]
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
        expected_state = {"resource_id": None, **state_to_enforce}
        changes = hub.tool.gcp.utils.compare_states(
            expected_state,
            ret["new_state"],
            RESOURCE_TYPE_SNAPSHOT,
            PARAMETER["read_only_props"],
        )

        assert not bool(changes), changes
    else:
        assert not ret["result"], ret["comment"]
        assert not ret["new_state"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                f"gcp.{RESOURCE_TYPE_SNAPSHOT}", name=PARAMETER["snapshot_name"]
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="dont_update_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_snapshot_present_dont_update_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, __test
):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **LABELS_TO_ADD,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_SNAPSHOT,
        state_to_enforce,
        __test,
        ["--get-resource-only-with-resource-id"],
    )
    assert not ret["old_state"]

    if __test:
        assert ret["result"], ret["comment"]
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
        expected_state = {"resource_id": None, **state_to_enforce}
        changes = hub.tool.gcp.utils.compare_states(
            expected_state,
            ret["new_state"],
            RESOURCE_TYPE_SNAPSHOT,
            PARAMETER["read_only_props"],
        )

        assert not bool(changes), changes
    else:
        assert not ret["result"], ret["comment"]
        assert not ret["new_state"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                f"gcp.{RESOURCE_TYPE_SNAPSHOT}", name=PARAMETER["snapshot_name"]
            )
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="snapshot_add_labels",
    depends=["present_create"],
)
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_snapshot_add_labels(hub, idem_cli, __test):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **LABELS_TO_ADD,
    }

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, state_to_enforce, __test
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )

    expected_new_state = {"resource_id": PARAMETER["resource_id"], **state_to_enforce}
    changes = hub.tool.gcp.utils.compare_states(
        expected_new_state,
        ret["new_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes

    expected_old_state = {
        "resource_id": PARAMETER["resource_id"],
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
    }
    changes = hub.tool.gcp.utils.compare_states(
        expected_old_state,
        ret["old_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="update_get_resource_only_with_resource_id_missing_resource_id",
    depends=["snapshot_add_labels"],
)
def test_snapshot_present_update_get_resource_only_with_resource_id_with_resource_id(
    hub, idem_cli, __test
):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "resource_id": PARAMETER["resource_id"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **CHANGED_LABELS,
    }

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_SNAPSHOT,
        state_to_enforce,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )

    expected_new_state = {"resource_id": PARAMETER["resource_id"], **state_to_enforce}
    changes = hub.tool.gcp.utils.compare_states(
        expected_new_state,
        ret["new_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes

    expected_old_state = {
        "resource_id": PARAMETER["resource_id"],
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **LABELS_TO_ADD,
    }
    changes = hub.tool.gcp.utils.compare_states(
        expected_old_state,
        ret["old_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="snapshot_labels_unchanged",
    depends=["update_get_resource_only_with_resource_id_missing_resource_id"],
)
async def test_snapshot_labels_unchanged(hub, idem_cli, __test):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "resource_id": PARAMETER["resource_id"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **CHANGED_LABELS,
    }

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, state_to_enforce, __test
    )

    assert (
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
            name=PARAMETER["snapshot_name"],
        )
        in ret["comment"]
    )

    expected_new_state = {"resource_id": PARAMETER["resource_id"], **state_to_enforce}
    changes = hub.tool.gcp.utils.compare_states(
        expected_new_state,
        ret["new_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes

    expected_old_state = {
        "resource_id": PARAMETER["resource_id"],
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **CHANGED_LABELS,
    }
    changes = hub.tool.gcp.utils.compare_states(
        expected_old_state,
        ret["old_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="snapshot_change_labels",
    depends=["snapshot_labels_unchanged"],
)
async def test_snapshot_change_labels(hub, idem_cli, __test):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "resource_id": PARAMETER["resource_id"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **LABELS_TO_CHANGE_AND_ADD,
    }

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, state_to_enforce, __test
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )

    expected_new_state = {"resource_id": PARAMETER["resource_id"], **state_to_enforce}
    changes = hub.tool.gcp.utils.compare_states(
        expected_new_state,
        ret["new_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes

    expected_old_state = {
        "resource_id": PARAMETER["resource_id"],
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **CHANGED_LABELS,
    }
    changes = hub.tool.gcp.utils.compare_states(
        expected_old_state,
        ret["old_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="snapshot_remove_labels",
    depends=["snapshot_change_labels"],
)
async def test_snapshot_remove_labels(hub, idem_cli, __test):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "resource_id": PARAMETER["resource_id"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **LABELS_TO_REMOVE,
    }

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, state_to_enforce, __test
    )

    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )

    expected_new_state = {"resource_id": PARAMETER["resource_id"], **state_to_enforce}
    changes = hub.tool.gcp.utils.compare_states(
        expected_new_state,
        ret["new_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes

    expected_old_state = {
        "resource_id": PARAMETER["resource_id"],
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **LABELS_TO_CHANGE_AND_ADD,
    }
    changes = hub.tool.gcp.utils.compare_states(
        expected_old_state,
        ret["old_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="snapshot_change_non_updatable_properties",
    depends=["snapshot_remove_labels"],
)
async def test_snapshot_change_non_updatable_properties(hub, idem_cli, __test):
    state_to_enforce = {
        "name": PARAMETER["snapshot_name"],
        "resource_id": PARAMETER["resource_id"],
        **CHANGED_NON_UPDATABLE_PROPERTIES,
    }

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, state_to_enforce, __test
    )
    assert not ret["result"], ret["comment"]
    assert ret["new_state"] == ret["old_state"]

    assert [
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
            name=PARAMETER["snapshot_name"],
            non_updatable_properties=set(CHANGED_NON_UPDATABLE_PROPERTIES.keys()),
        )
    ] == ret["comment"]

    expected_old_state = {
        "resource_id": PARAMETER["resource_id"],
        "name": PARAMETER["snapshot_name"],
        "source_disk": PARAMETER["source_disk"],
        **SNAPSHOT_CREATE_PROPERTIES,
        **LABELS_TO_CHANGE_AND_ADD,
    }
    changes = hub.tool.gcp.utils.compare_states(
        expected_old_state,
        ret["old_state"],
        RESOURCE_TYPE_SNAPSHOT,
        PARAMETER["read_only_props"],
    )
    assert not bool(changes), changes


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_snapshot_delete(hub, ctx, idem_cli, __test):

    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.snapshot",
        PARAMETER["snapshot_name"],
        PARAMETER["resource_id"],
        test=__test,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state")
    assert not ret.get("new_state")

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_SNAPSHOT}",
                name=PARAMETER["snapshot_name"],
            )
            in ret["comment"]
        )
        ret = await hub.exec.gcp.compute.snapshot.get(
            ctx, resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"]
        assert not ret["ret"]


@pytest.mark.asyncio
async def test_snapshot_describe(hub, ctx):
    ret = await hub.states.gcp.compute.snapshot.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.snapshot.present")
        assert described_resource
        snapshot = dict(ChainMap(*described_resource))
        assert snapshot.get("resource_id") == resource_id
        assert hub.tool.gcp.resource_prop_utils.parse_link_to_resource_id(
            resource_id, "compute.snapshot"
        )


@pytest.mark.asyncio
async def test_snapshot_full_url(hub, idem_cli):
    source_disk_resource_id = PARAMETER["source_disk"]
    create_state = {
        "name": generate_unique_name("idem-test-snapshot"),
        "source_disk": f"https://www.googleapis.com/compute/v1/{source_disk_resource_id}",
        "labels": {"abc": "hello"},
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, create_state
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"], ret["comment"]

    snapshot = ret["new_state"]
    assert create_state["source_disk"] != snapshot["source_disk"]
    assert create_state["source_disk"].endswith(snapshot["source_disk"])
    assert snapshot["source_disk"] == source_disk_resource_id

    update_state = {
        **create_state,
        "labels": {"abc": "hello123"},
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, update_state
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["comment"]
    assert ret["old_state"]["labels"] == create_state["labels"]
    assert ret["new_state"], ret["comment"]
    assert ret["new_state"]["labels"] == update_state["labels"]

    hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE_SNAPSHOT, create_state["name"], snapshot["resource_id"]
    )
