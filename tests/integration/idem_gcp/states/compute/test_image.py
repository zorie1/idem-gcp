from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-image"),
    "invalid_resource_id": "/projects/tango-gcp/global/images/invalid-name",
}
RESOURCE_TYPE_IMAGE = "compute.image"
PRESENT_STATE_IMAGE = {
    "name": PARAMETER["name"],
}


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.asyncio
def test_present_no_op(hub, idem_cli, __test):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_IMAGE, PRESENT_STATE_IMAGE, __test
    )
    assert ret
    assert ret["result"]
    assert ret["comment"]
    assert ret["comment"] == [
        "No-op: There is no create/update function for gcp.compute.image"
    ]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_absent_invalid_resource_id(hub, idem_cli, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_IMAGE,
        PARAMETER["name"],
        PARAMETER["invalid_resource_id"],
        test=__test,
    )

    assert ret
    assert ret.get("comment")

    absent_comment = hub.tool.gcp.comment_utils.already_absent_comment(
        "gcp.compute.image", PARAMETER["name"]
    )
    assert any(absent_comment in c for c in ret["comment"]), ret["comment"]


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.image.describe(ctx)
    for resource_id in ret:
        described_resource = ret[resource_id].get("gcp.compute.image.present")
        assert described_resource
        image = dict(ChainMap(*described_resource))
        assert image.get("resource_id") == resource_id
