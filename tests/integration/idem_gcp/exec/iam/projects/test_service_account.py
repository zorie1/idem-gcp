import time
from typing import Any
from typing import Dict
from typing import List

import pytest

from tests.utils import generate_unique_name

EXEC_SA_SLS = """
random-name:
  exec.run:
  - path: gcp.iam.projects.service_account.{api_method}
  - kwargs:
      resource_id: {resource_id}
"""


@pytest.fixture(scope="module")
async def gcp_service_accounts(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.iam.projects.service_account.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.fixture(scope="module")
async def service_account(hub, ctx, cleaner):
    project = hub.tool.gcp.utils.get_project_from_account(ctx, None)

    create_ret = await hub.exec.gcp_api.client.iam.projects.service_account.create(
        ctx,
        name=f"projects/{project}",
        body={
            "account_id": generate_unique_name("idem-gcp", 30),
            "service_account": {
                "description": f"Created by idem-gcp {test_disable_enable_account.__name__} integration test."
            },
        },
    )
    assert create_ret["result"], create_ret["comment"]
    assert create_ret["ret"]

    service_account = create_ret["ret"]

    for _ in range(3):
        get_ret = await hub.exec.gcp.iam.projects.service_account.get(
            ctx, resource_id=service_account["resource_id"]
        )
        if not get_ret["result"]:
            time.sleep(2)
        else:
            break

    yield service_account

    cleaner(service_account, "iam.projects.service_account")


@pytest.mark.asyncio
async def test_list_for_project(hub, ctx):
    ret = await hub.exec.gcp.iam.projects.service_account.list(ctx)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) > 0


@pytest.mark.asyncio
async def test_list_invalid_project(hub, ctx):
    ret = await hub.exec.gcp.iam.projects.service_account.list(
        ctx, project="invalid-project"
    )
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = f"projects/{ctx.acct.project_id}/serviceAccounts/some-name"
    ret = await hub.exec.gcp.iam.projects.service_account.get(
        ctx, resource_id=invalid_resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert any("result is empty" in c for c in ret["comment"])


@pytest.mark.asyncio
async def test_get_invalid_unique_id(hub, ctx):
    ret = await hub.exec.gcp.iam.projects.service_account.get(ctx, unique_id="no-name")
    assert ret["result"], not ret["ret"]
    assert (
        "Get iam.projects.service_account "
        "'projects/tango-gcp/serviceAccounts/no-name' result is empty" in ret["comment"]
    )


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_service_accounts):
    if len(gcp_service_accounts) > 0:
        service_account = gcp_service_accounts[0]

        ret = await hub.exec.gcp.iam.projects.service_account.get(
            ctx,
            resource_id=service_account["resource_id"],
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_service_account = ret["ret"]
        assert service_account["project_id"] == returned_service_account["project_id"]
        assert service_account["email"] == returned_service_account["email"]
        assert service_account.get("resource_id") == returned_service_account.get(
            "resource_id"
        )


@pytest.mark.asyncio
async def test_get_by_project_and_email(hub, ctx, gcp_service_accounts):
    if len(gcp_service_accounts) > 0:
        service_account = gcp_service_accounts[0]

        ret = await hub.exec.gcp.iam.projects.service_account.get(
            ctx, project=service_account["project_id"], email=service_account["email"]
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_service_account = ret["ret"]
        assert service_account["project_id"] == returned_service_account["project_id"]
        assert service_account["email"] == returned_service_account["email"]
        assert service_account.get("resource_id") == returned_service_account.get(
            "resource_id"
        )


@pytest.mark.asyncio
async def test_get_by_unique_id(hub, ctx, gcp_service_accounts):
    if len(gcp_service_accounts) > 0:
        service_account = gcp_service_accounts[0]

        ret = await hub.exec.gcp.iam.projects.service_account.get(
            ctx, unique_id=service_account["unique_id"]
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_service_account = ret["ret"]
        assert service_account["project_id"] == returned_service_account["project_id"]
        assert service_account["email"] == returned_service_account["email"]
        assert service_account.get("resource_id") == returned_service_account.get(
            "resource_id"
        )


@pytest.mark.asyncio
async def test_undelete(hub, ctx, cleaner):
    project = hub.tool.gcp.utils.get_project_from_account(ctx, None)

    create_ret = await hub.exec.gcp_api.client.iam.projects.service_account.create(
        ctx,
        name=f"projects/{project}",
        body={
            "account_id": generate_unique_name("idem-gcp", 30),
            "service_account": {
                "description": f"Created by idem-gcp {test_undelete.__name__} integration test."
            },
        },
    )
    assert create_ret["result"], create_ret["comment"]
    assert create_ret["ret"]

    service_account = create_ret["ret"]

    time.sleep(5)
    delete_ret = await hub.exec.gcp_api.client.iam.projects.service_account.delete(
        ctx, name=service_account["resource_id"]
    )
    assert delete_ret["result"], delete_ret["comment"]

    time.sleep(5)
    get_ret = await hub.exec.gcp.iam.projects.service_account.get(
        ctx, resource_id=service_account["resource_id"]
    )
    assert get_ret["result"], get_ret["comment"]
    assert not get_ret["ret"]
    assert any("result is empty" in c for c in get_ret["comment"])

    undelete_ret = await hub.exec.gcp.iam.projects.service_account.undelete(
        ctx, unique_id=service_account["unique_id"]
    )
    assert undelete_ret["result"], undelete_ret["comment"]

    time.sleep(5)
    get_ret = await hub.exec.gcp.iam.projects.service_account.get(
        ctx, resource_id=service_account["resource_id"]
    )
    assert get_ret["result"], get_ret["comment"]
    assert get_ret["ret"]

    cleaner(service_account, "iam.projects.service_account")


@pytest.mark.asyncio
async def test_disable_enable_account(hub, ctx, idem_cli, service_account):
    state = EXEC_SA_SLS.format(
        resource_id=service_account["resource_id"], api_method="disable"
    )
    disable_ret = hub.tool.utils.call_exec_from_sls(idem_cli, state)
    assert disable_ret["result"], disable_ret["comment"]

    get_ret = await hub.exec.gcp.iam.projects.service_account.get(
        ctx, resource_id=service_account["resource_id"]
    )
    assert get_ret["result"], get_ret["comment"]
    assert "disabled" in get_ret["ret"]
    assert get_ret["ret"]["disabled"] is True

    state = EXEC_SA_SLS.format(
        resource_id=service_account["resource_id"], api_method="enable"
    )
    enable_ret = hub.tool.utils.call_exec_from_sls(idem_cli, state)
    assert enable_ret["result"], enable_ret["comment"]

    get_ret = await hub.exec.gcp.iam.projects.service_account.get(
        ctx, resource_id=service_account["resource_id"]
    )
    assert get_ret["result"], get_ret["comment"]
    assert "disabled" not in get_ret["ret"]
