from typing import Any
from typing import Dict
from typing import List

import pytest

from tests.utils import generate_unique_name

PARAMETER = {
    "snapshot_name": generate_unique_name("idem-test-snapshot"),
    "disk_name": generate_unique_name("idem-test-disk"),
}

SNAPSHOT_SPEC = """
{name}:
  gcp.compute.snapshot.present:
  - name: {name}
  - source_disk: {disk}
  - storage_locations:
    - us-central1
"""


@pytest.fixture(scope="function")
def test_disk(hub, idem_cli, tests_dir):
    global PARAMETER

    if "test_disk" in PARAMETER:
        return PARAMETER["test_disk"]

    path_to_sls = tests_dir / "sls" / "compute" / "default_disk_present.sls"
    with open(path_to_sls) as f:
        sls_str = f.read()
        sls_str = sls_str.format(
            **{
                "name": PARAMETER["disk_name"],
                "size_gb": "1",
                "resource_policies": [],
                "labels": {},
                "label_fingerprint": "",
            }
        )
        ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str)
        assert ret["result"], ret["comment"]

        disk = ret["new_state"]
        assert disk

        PARAMETER["test_disk"] = disk

        return disk


@pytest.fixture(scope="function")
def test_snapshot(hub, ctx, idem_cli, test_disk):
    global PARAMETER

    if "test_snapshot" in PARAMETER:
        return PARAMETER["test_snapshot"]

    sls_str = SNAPSHOT_SPEC.format(
        **{"name": PARAMETER["snapshot_name"], "disk": test_disk.get("resource_id")}
    )
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str)
    assert ret["result"], ret["comment"]
    assert ret["new_state"]

    snapshot = ret["new_state"]
    PARAMETER["test_snapshot"] = snapshot


@pytest.fixture(scope="module")
async def gcp_snapshots(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.snapshot.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_filter(hub, ctx, gcp_snapshots):
    if not gcp_snapshots:
        return

    snapshot = gcp_snapshots[0]

    ret = await hub.exec.gcp.compute.snapshot.list(
        ctx,
        filter_=f"name = {snapshot['name']}",
    )

    assert ret["ret"]
    assert ret["result"]
    assert len(ret["ret"]) == 1

    ret = await hub.exec.gcp.compute.snapshot.list(
        ctx,
        filter_=f"name = {snapshot['name']}0",
    )

    assert not ret["ret"]
    assert ret["result"]


@pytest.mark.asyncio
async def test_list_filter_regex(hub, ctx, gcp_snapshots):
    if not gcp_snapshots:
        return

    snapshot = gcp_snapshots[0]
    name_substr = snapshot["name"]
    if len(name_substr) > 1:
        name_substr = name_substr[1:]

    ret = await hub.exec.gcp.compute.snapshot.list(
        ctx,
        filter_=f"name eq '.*{name_substr}'",
    )

    assert ret["ret"]
    assert ret["result"]
    assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, gcp_snapshots):
    if not gcp_snapshots:
        return

    snapshot = gcp_snapshots[0]

    ret = await hub.exec.gcp.compute.snapshot.get(
        ctx,
        name=f"{snapshot['name']}",
    )

    assert ret["result"]
    assert ret["ret"]
    assert not ret["comment"]

    returned_snapshot = ret["ret"]
    assert returned_snapshot == snapshot


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, gcp_snapshots):
    if not gcp_snapshots:
        return

    snapshot = gcp_snapshots[0]

    ret = await hub.exec.gcp.compute.snapshot.get(
        ctx,
        resource_id=snapshot["resource_id"],
    )

    assert ret["result"]
    assert ret["ret"]
    assert not ret["comment"]

    returned_snapshot = ret["ret"]
    assert returned_snapshot == snapshot


@pytest.mark.asyncio
async def test_snapshot_set_labels(hub, ctx, test_snapshot):
    global PARAMETER

    snapshot = PARAMETER["test_snapshot"]
    assert not snapshot.get("labels")

    labels = {
        "label1": "value1",
        "label2": "value2",
    }

    ret = await hub.exec.gcp.compute.snapshot.set_labels(
        ctx,
        resource_id=snapshot["resource_id"],
        labels=labels,
        label_fingerprint=snapshot.get("label_fingerprint"),
    )

    assert ret["result"]
    assert ret["ret"]
    assert not ret["comment"]

    assert ret["ret"].get("labels") == labels


@pytest.mark.asyncio
async def test_cleanup(hub, ctx, idem_cli, test_snapshot):
    global PARAMETER

    ret = hub.tool.utils.call_absent(
        idem_cli,
        "compute.snapshot",
        PARAMETER["snapshot_name"],
        PARAMETER["test_snapshot"].get("resource_id"),
    )

    assert ret["result"]
