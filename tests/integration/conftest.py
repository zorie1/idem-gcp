import asyncio
import hashlib
import os
import sys
from typing import Any
from typing import Callable
from typing import Dict
from typing import List

import dict_tools
import pop
import pytest
import pytest_asyncio
import pytest_idem.runner

# ================================================================================
# pop fixtures
# ================================================================================


SUPPORTED_PYTHON_VERSIONS = [(3, 7), (3, 8), (3, 9), (3, 10)]


@pytest.fixture(scope="module", autouse=True)
def conditionally_skip_integration_test_module(request) -> None:
    pipeline_id = os.environ.get("CI_PIPELINE_ID")
    tag_id = os.environ.get("CI_COMMIT_TAG")
    if not pipeline_id or tag_id:
        # Unconditionally run all tests when 1) running locally or 2) making a release
        return
    test_module_file_path: str = request.fspath.strpath.split("idem_gcp")[-1]
    test_module_key = f"{pipeline_id}:{test_module_file_path}"
    v = hashlib.sha256(test_module_key.encode())
    h = int.from_bytes(v.digest()[:4], "little")
    run_on = SUPPORTED_PYTHON_VERSIONS[h % 4]
    if run_on != sys.version_info[:2]:
        pytest.skip(
            f"Test module '{test_module_file_path}' will run on Python {'.'.join(map(str, run_on))}",
            allow_module_level=True,
        )


@pytest.fixture(scope="module")
def acct_data(ctx):
    """
    acct_data that can be used in running simple yaml blocks
    """
    yield {"profiles": {"gcp": {"default": ctx.acct}}}


@pytest.fixture(scope="module", autouse=True)
def acct_subs() -> List[str]:
    return ["gcp"]


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_gcp"


@pytest.fixture(scope="module", autouse=True)
def gcp_project(ctx) -> str:
    return ctx.acct.project_id


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="hub")
def integration_hub(tests_dir, event_loop):
    hub = pop.hub.Hub()
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.config.load(["idem", "acct", "idem_gcp"], cli="idem", parse_cli=False)

    hub.idem.RUNS = {"test": {}}
    yield hub


@pytest_asyncio.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(
        run_name="test",
        test=False,
        tag="fake_|-test_|-tag",
        old_state={},
    )

    if not hub.OPT.acct.acct_file:
        pytest.fail(
            "Missing acct_file. Add ACCT_FILE to your environment with the path to your encrypted credentials file",
            False,
        )
    if not hub.OPT.acct.acct_key:
        pytest.fail(
            "Missing acct_key. Add ACCT_KEY to your environment with the fernet key to decrypt your credentials file",
            False,
        )

    # Add the profile to the account
    await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    if not hub.acct.UNLOCKED:
        pytest.fail(f"acct could not unlock {hub.OPT.acct.acct_file}")

    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)

    yield ctx


@pytest.fixture(scope="module")
def idem_cli() -> Callable:
    return pytest_idem.runner.idem_cli


@pytest.fixture(scope="module", autouse=True)
def cleaner(hub, ctx, idem_cli):
    resources_to_delete = {}

    def _clean(res: Dict[str, Any], res_type: str):
        resources_to_delete[res.get("resource_id")] = {
            "name": res.get("name"),
            "type": res_type,
        }

    yield _clean

    for resource_id, resource_info in resources_to_delete.items():
        ret = hub.tool.utils.call_absent(
            idem_cli,
            resource_info.get("type"),
            resource_info.get("name"),
            resource_id,
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")


# --------------------------------------------------------------
