Describe
========

The *Describe* command will show you how your environment is currently
configured. The output describing your environment can also be used to
configure and update your environment.

Examples
++++++++

Networks
--------

List and Describe your networks:

.. code-block:: bash

    idem describe gcp.compute.network

Output:

.. code-block:: sls

    projects/tango-gcp/global/networks/idem-test-network-1cb65e71-9eb8-4202-8cb0-a5c22e6a2fcc:
      gcp.compute.network.present:
      - name: idem-test-network-1cb65e71-9eb8-4202-8cb0-a5c22e6a2fcc
      - auto_create_subnetworks: false
      - peerings:
        - auto_create_routes: true
          exchange_subnet_routes: true
          export_custom_routes: false
          export_subnet_routes_with_public_ip: true
          import_custom_routes: false
          import_subnet_routes_with_public_ip: false
          name: idem-test-peering-2c97466c-4da9-4bb7-926c-8d4655e5c259
          network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/idem-test-network-af9f075f-6b4a-4f21-b823-eda06b1d14e7
          stack_type: IPV4_ONLY
          state: ACTIVE
          state_details: '[2023-02-16T03:36:26.805-08:00]: Connected.'
      - routing_config:
          routing_mode: REGIONAL
      - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
      - resource_id: projects/tango-gcp/global/networks/idem-test-network-1cb65e71-9eb8-4202-8cb0-a5c22e6a2fcc

The above example shows info about each network. In the next page discussing the
`Idem State </quickstart/commands/state>`_ we'll show how you can use the
output of the `Describe` command to configure the state of existing resources.

Instances
---------

List and Describe your GCP Instances

.. code-block:: bash

    idem describe gcp.compute.instance

Output:

.. code-block:: sls

    projects/tango-gcp/zones/us-central1-a/instances/idem-test-instance-e360e048-8126-4fa5-a41f-7eb04788c25e:
      gcp.compute.instance.present:
      - name: idem-test-instance-e360e048-8126-4fa5-a41f-7eb04788c25e
      - tags:
          fingerprint: 42WmSpB8rSM=
      - machine_type: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/machineTypes/e2-micro
      - status: STOPPING
      - zone: us-central1-a
      - network_interfaces:
        - fingerprint: 5nEUL6IlOr0=
          kind: compute#networkInterface
          name: nic0
          network: https://www.googleapis.com/compute/v1/projects/tango-gcp/global/networks/default
          network_ip: 10.128.0.121
          stack_type: IPV4_ONLY
          subnetwork: https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/subnetworks/default
      - disks:
        - auto_delete: false
          boot: true
          device_name: persistent-disk-0
          disk_size_gb: '1'
          index: 0
          interface: SCSI
          kind: compute#attachedDisk
          mode: READ_WRITE
          source: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/disks/idem-test-disk-7c0ba92d-e0dc-44e3-93d1-3cc129650efd
          type: PERSISTENT
      - metadata:
          fingerprint: zrn-PNxgE6M=
          kind: compute#metadata
      - scheduling:
          automatic_restart: true
          on_host_maintenance: MIGRATE
          preemptible: false
          provisioning_model: STANDARD
      - label_fingerprint: 42WmSpB8rSM=
      - deletion_protection: false
      - fingerprint: lF5m-IjAhOc=
      - resource_id: projects/tango-gcp/zones/us-central1-a/instances/idem-test-instance-e360e048-8126-4fa5-a41f-7eb04788c25e
