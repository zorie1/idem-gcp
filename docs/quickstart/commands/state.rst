State
=====

The *State* command allows you to ensure your environment is configured how you
specify. You specify your configuration in an *SLS* file, also known as a
*state* file.

Describe Example
++++++++++++++++

First let's describe our disks.

.. code-block:: bash

    idem describe gcp.compute.disk

Output:

.. code-block:: sls

    projects/tango-gcp/zones/us-central1-a/disks/my-disk-1:
      gcp.compute.disk.present:
      - name: my-disk-1
      - size_gb: '10'
      - zone: us-central1-a
      - type: https://www.googleapis.com/compute/v1/projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
      - label_fingerprint: 42WmSpB8rSM=
      - physical_block_size_bytes: '4096'
      - resource_policies:
        - https://www.googleapis.com/compute/v1/projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1
      - resource_id: projects/tango-gcp/zones/us-central1-a/disks/my-disk-1

Use Idem State
______________

You'll notice that `size_gb` is set to `10`. If I want to
change that value to `20` I can take the above output from the Describe
example and create a file with `.sls` extension and use that to update the
running configuration.

Copy the above text output to ~/my_disk.sls, change `size_gb` to
`20` and then run the following command:

.. code-block:: bash

    idem state ~/my_disk.sls

Output:

.. code-block:: yaml

          ID: projects/tango-gcp/zones/us-central1-a/disks/my-disk-1
    Function: gcp.compute.disk.present
      Result: True
     Comment: ("Updated gcp.compute.disk 'my-disk-1'",)
     Changes:
    old:
        ----------
        size_gb:
            10
    new:
        ----------
        size_gb:
            20

Here you'll notice that the `old` field shows us the previous value for
`size_gb` and the `new` field shows us the new value.
